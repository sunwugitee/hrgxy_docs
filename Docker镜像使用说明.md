# 镜像使用说明

镜像分为编译构建qemu_small_system_demo方案镜像和qemu测试镜像, 由于镜像只是自用时为了方便, 在移植运行时不可避免会出现其它问题, 出现问题望及时反馈以便进行更新.

镜像上传到阿里云镜像仓库上, 本文对镜像的使用方法进行简单说明.

## docker安装

使用阿里云脚本安装, 详细教程[如下](https://developer.aliyun.com/article/110806)

1. 执行阿里云安装脚本(手动安装请查看详细教程)

   ```shell
   curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
   ```

2. 配置用户组(可选,)

   在进行用户组配置后, 执行docker命令时不需要再添加sudo, 方便执行调试

   ```shell
   sudo groupadd docker
   sudo usermod -aG docker ${USER}
   sudo systemctl restart docker
   newgrp - docker
   ```

3. 检查docker是否正常安装

   ```shell
   docker --version
   
   # Docker version 20.10.14, build a224086							# 安装成功
   ```

4. 配置阿里云容器镜像加速服务(可选)

   配置该服务时为了加速镜像下载速度, 可以不配置, 官方详细配置教程[如下](https://help.aliyun.com/document_detail/60750.html)

   > 简要流程为: 登录[容器镜像服务控制台](https://cr.console.aliyun.com/?spm=a2c4g.11186623.0.0.19e67627FNRExD)，在左侧导航栏选择镜像工具 > 镜像加速器，在镜像加速器页面获取镜像加速地址, 获取地址的同时也有教程提示如何使用加速地址.



## 获取镜像

在成功安装docker后, 可以获取build和test镜像用于源码编译和测试.

1. 获取编译镜像

   ```bash
   docker pull registry.cn-hangzhou.aliyuncs.com/zhangxh98/build:1.0
   ```

2. 获取测试镜像

   ```shell
   docker pull registry.cn-hangzhou.aliyuncs.com/zhangxh98/test:1.0
   ```



## 镜像使用说明

1. 运行编译容器命令

   ```shell
   # 命令只提供示例, 用于说明讲解, 使用时根据自身情况进行更改
   docker run -it -v /home/ubuntu/ohos3.0/:/home/ubuntu/ohos3.0 registry.cn-hangzhou.aliyuncs.com/zhangxh98/build:1.0 /bin/bash
   
   # 挂载目录为源码目录, 如果源码在本机上时需要将源码挂载到容器中进行编译, 这里两个目录必须保持一致, 否则后续的编译会报错
   # 也可以在容器中下载源码, 脱离主机环境进行编译测试, 这样就不建议使用qemu_test镜像, 而是在编译容器中安装qemu, 不然两个容器之间没有挂载目录通信, 联合开发十分不方便
   ```

2. 运行测试容器命令

   ```shell
   ocker run -it -v /home/ubuntu/ohos3.0/:/home/ubuntu/ohos3.0 -p 5920:5920 -p 1234:1234 registry.cn-hangzhou.aliyuncs.com/zhangxh98/test:1.0 /bin/bash
   
   # 示例中仍挂载了源码目录, 和上一个命令一样, 可根据自身情况进行选择
   # 这里开放了1234端口时为了能够在本机使用gdb调试qemu
   ```

3. 其他说明(可选)

   这里推荐使用vscode 的 remote-contianer 插件对容器进行管理, 虽然上述开发模式的开发流程为:

   >  本机: 阅读/修改代码 -> 编译容器编译 ->测试容器测试

   但是如果想将开发流程完全放在容器中, vscode是一个很好的帮手.

