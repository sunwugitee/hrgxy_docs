# Qemu使用文档

本文档对编译 qemu_small_system_demo 方案并在qemu上运行测试流程进行简单总结, [官方文档](https://gitee.com/openharmony/device_qemu)中有详细的安装说明, 对于运行测试教程可以参考[Qemu for litesos_a](https://gitee.com/openharmony/device_qemu/blob/HEAD/arm_virt/liteos_a/README_zh.md),  这里主要是承接 [环境搭建文档](https://gitee.com/hrgxy/openharmony_docs/blob/master/%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E6%96%87%E6%A1%A3.md) 进行后续的使用说明.



## 运行测试

在环境搭建成功, 并成功编译 qemu_small_system_arm 方案后, 执行如下步骤:

1. 安装 qemu 运行所需依赖

   ```shell
   sudo apt install build-essential zlib1g-dev pkg-config libglib2.0-dev  binutils-dev libboost-all-dev autoconf libtool libssl-dev libpixman-1-dev virtualenv flex bison
   ```

2. 安装qemu

   ```shell
   sudo apt install qemu-system-arm
   
   # 注:若本机Ubuntu版本小于20.04, 采用此种方式安装的qemu可能由于版本较低而不支持 cortex-a7 架构
   # 需要自行采用源码编译安装, 安装教程如下: https://gitee.com/openharmony/device_qemu/blob/master/README_zh.md
   ```

3. 进入源码根目录, 运行官方脚本启动qemu

   ```shell
   ./qemu-run
   
   # 运行过程中可能提示smallmcc.img相关错误, 说明mmc镜像不能正常创建, 大概率是因为本机缺少loop设备而不能正常制作mmc镜像导致, 可采用挂载代码测试或者在gitee仓中下载
   ```





## 服务器无法运行 qemu-run 的解决方案 (使用全量版本代码)

注:目前由于全量代码使用qemu存在问题, 故不推荐使用全量代码测试, 固定版本代码经测试能够在服务器上运行qemu, 如果有问题望及时反馈

#### 服务器端进行编译 + 运行qemu所有操作

服务器端不能运行qemu的原因在于缺少loop设备，无法制作smallmmc.img，可以下载目录中的对应文件到源码目录下的 out/ 文件夹中，然后即可正常运行qemu



#### 服务器端进行编译 + 本地进行运行测试

采用此种方式，可以在本地下载源码备份， 当服务器端编译完成后，可将编译文件下载到本地，替换掉本地源码备份的对应文件(如out/arm_virt)，这样即可在本地的源码目录执行qemu-run



#### 本地编译+本地测试

所有流程均在本地完成即可



#### 挂载代码测试(可选)

**注: 由于采用此种方式制作smallmmc.img对于带宽要求极高，等待时间较长，该方法仅供相关用途参考，不建议使用**

如果在服务器上, 因为相关/dev设备的缺失不能正常进行qemu的运行测试, 可以通过如下方法将服务器上的代码目录挂载到本地 , 然后在本地运行qemu进行测试, 相关流程简要说明如下:

1. 本机环境要求:Ubuntu

2. 挂载服务器目录到本地

   ```shell
   1. 安装ssh fs挂载工具
   sudo apt install sshfs
   
   2. 配置用户权限
   # 远程挂载主机与本机均需配置
   sudo vim /etc/fuse.cong
   打开user_allow_other选项
   
   3. 将远程目录挂载到本地, 目录仅作参考
   sshfs -p 10xxx -o allow_other ubuntu@192.168.xx.xx:/home/ubuntu/openharmony /home/ubuntu/openharmony
   
   # 这里可能需要输入ssh登录密码
   
   # 卸载时直接使用 umount 卸载即可
   ```

3. 检查qemu运行环境

   ```shell
   1. 检查本机python版本, 要求python3.8及以上
   这里必须是/usr/bin/python3 的版本大于3.8
   
   2. 检查本机是否安装hb编译工具
   若没有hb则按照 环境搭建教程->编译准备 中在本机安装hb
   
   3. 检查本机qemu-system-arm版本, 要求qemu-system-arm版本在4.2以上
   若版本较低或者没有安装qemu, 则根据自己的ubuntu版本选取apt安装或者源码安装
   ```

4. 进入源码根目录运行

   ```shell
   ./qemu-run -f											# -f 命令会强制删除falsh.img, 重新制作系统镜像, 根据需要选择参数
   ```

   

## 其他说明

1. 该文档的使用建立在环境搭建文档中的相关步骤正常完成, 如果不能够得到编译成功则仍需要环境配置才能进行下一步
2. 这里安装qemu 时只安装了 arm的虚拟机, 可以根据需要自行安装其他架构的虚拟机进行相关测试
3. 服务器上运行时可能会遇到因为/dev/目录下没有对应设备而无法挂载文件系统的错误, 只要命令行输出中有内核正常启动的调试信息, 则说明运行正常
4. **在执行qemu-run时可能会遇到各种关于loop设备的问题，如果不能解决，望及时反馈汇总**

