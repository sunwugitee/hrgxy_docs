# 香蕉派使用文档说明

测试香蕉派分为使用 sunxi-tools工具利用 USB 线远程加载内核，和将内核烧写到 SD 卡中两种，本文会对这两种情况进行相关说明。

> **环境要求：**

**Ubuntu** 20.04 及以上

**repo** 2.8

**python** 3.8 及 以上（3.10由于库引用目录的变化可能需要对部分地方进行修改）

**arm-linux-gnueabi-gcc** 11.2 (不要太低，否则编译u-boot会出问题)



**鸿蒙编译环境配置参考**：[环境搭建文档.md · 哈工轩辕/openharmony_docs - 码云 - 开源中国 (gitee.com)](https://gitee.com/hrgxy/openharmony_docs/blob/master/环境搭建文档.md)

其中包含对于 **repo工具** 和 鸿蒙编译工具 **hb** 工具的安装方法，**hb 工具可以在源码下载后安装**

## 使用USB线远程加载内核

### 源码下载：

```shell
0. 创建文件夹
mkdir bananapi_code
cd bananapi_code 						# 源码下载所有命令均在该文件夹中执行

1. 安装好repo工具后进行源码下载
repo init -u https://gitee.com/hrgxy/manifest.git -b mmc_stable -m app_dev_fixed.xml
repo sync
repo forall -c 'git lfs pull'

# 注：目前相关代码已集成到最新的manifest中，该代码包含部分容器实现以及所有已经移植完成的驱动，也可下载该分支的代码，但是后续的编译命令需要替换
# 拉去代码命令：
repo init -u git@gitee.com:hrgxy/manifest -b container -m container.xml -g ohos:small
# 编译命令：
hb build -f --gn-args liteos_skip_make=true

2. 自动化配置鸿蒙编译环境
sudo bash build/prebuilts_download.sh

3. 检查默认命令行是否为bash
ls -al /bin/sh
# 正确结果: /bin/sh -> bash
# 错误结果: /bin/sh -> dash

# 如果默认命令行为 dash , 使用以下命令进行更换
sudo dpkg-reconfigure dash
弹窗选择 no 

# 重复检查命令确认结果

4. (可选,若之前没有安装hb)安装hb并更新至最新版
pip3 install build/lite					# 该命令同样需要在源码根目录下执行

vim ~/.bashrc							# 配置环境变量
export PATH=~/.local/bin:$PATH			# 将以下命令拷贝到.bashrc文件的最后一行，保存并退出
source ~/.bashrc

hb -h									# 验证hb是否安装成功
# 示例输出:
usage: hb [-h] [-v] {build,set,env,clean} ...

OHOS Build System version 0.4.6

positional arguments:
  {build,set,env,clean}
    build               Build source code
    set                 OHOS build settings
    env                 Show OHOS build env
    clean               Clean output

optional arguments:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
# 如下输出说明安装成功

#可采用以下命令卸载hb : pip3 uninstall ohos-build
```



### 环境配置

```shell
1. 进行简单的环境配置,具体的后续流程中如果缺少环境可以看上面的鸿蒙编译环境配置参考
sudo apt-get install pkg-config pkgconf zlib1g-dev libusb-1.0-0-dev arm-linux-gnueabi-gcc
sudo apt-get install libfdt-dev
sudo apt install swig
```



### 编译运行内核

**注** 该流程中仍在源码目录下执行

```shell
1. 下载 sunxi-tools并编译
git clone https://github.com/linux-sunxi/sunxi-tools
cd sunxi-tools
make -j`nproc`
cd ..

2. 下载uboot并编译
git clone --depth=1 -b v2022.01 https://gitee.com/mirrors/u-boot
cd u-boot
make Bananapi_M2_Ultra_defconfig		# 使用香蕉派默认配置
make menuconfig							# 需要对配置进行相关改动

"ARM architecture > Search (SEC) > Enable support for booting in non-secure mode"中的"Boot in secure mode by default"为y。（即CONFIG_ARMV7_BOOT_SEC_DEFAULT=y）

"ARM architecture"中的"Use LPAE page table format"为n。（即CONFIG_ARMV7_LPAE=n）

# 注: 这里也可不进行配置,直接将仓库里的.config文件下载到uboot目录即可

CROSS_COMPILE=arm-linux-gnueabi- make -j`nproc`

cd ..

# 注: win10 使用sunxi-fei工具需要安装额外驱动
# zadig工具下载地址：https://gitee.com/hrgxy/openharmony_docs/raw/master/bpi_m2_ultra/zadig-2.7.exe
# 工具使用说明：https://linux-sunxi.org/FEL/USBBoot#Using_sunxi-fel_on_Windows


## 运行初始内核说明（不加载文件系统）##
# 使用FEL模式运行u-boot和内核需要将开发板通过USB线连接到主机上
# 如果想要观察输出信息,还需要准备一根串口线将开发板连接到主机上
# 如果使用虚拟机需要注意USB设备的透传问题。

# 关于usb使用fel模式启动u-boot和镜像的说明可以参照：[https://linux-sunxi.org/FEL/USBBoot]
# BPI-M2U进入fel模式可以通过上电时按住调试串口旁的按钮，也可以按照[https://linux-sunxi.org/FEL]中"Triggering FEL mode -> Through a special SD card image"里的方法烧录SD卡。

3. 制作uImage -> 制作 boot_liteos.cmd
cat <<EOF > boot_liteos.cmd
bootm start
bootm loados
bootm go
EOF 

4. 制作uImage -> 制作引导所需文件头
./u-boot/tools/mkimage -T script -A arm -O linux -C none -a0 -e0 -n "boot_liteos_scr" -d boot_liteos.cmd boot.scr

5. 制作uImage -> 制作内核镜像
# OHOS_Image.bin为源码树下out文件下生成的内核二进制文件
./u-boot/tools/mkimage -A arm -O linux -T kernel -d out/bpi_m2_ultra/bpi_m2_ultra_demo/OHOS_Image.bin -C none -a 0x40000000 -e 0x40000020 uImage

6. (可选, 需要安装picocom)
# 首先需要确认 /dev/ttyUSB0 存在,之后另开一个命令行执行
sudo picocom -b 115200 /dev/ttyUSB0

7. 使用sunxi-fei工具启动内核
# 回到最开始的命令行
sudo ./sunxi-tools/sunxi-fel uboot ./u-boot/u-boot-sunxi-with-spl.bin write 0x42000000 uImage write 0x43100000 boot.scr

8. 内核正确启动应该在调试窗口看到如下信息, 后续可以利用网口进行文件系统的启动
*******************************************

main core booting up...
cpu 0 entering scheduler
dev mem init ...
DeviceManagerStart start ...
SunximciMmcInit: proc init success.
SunximciMmcInit: success.
FUNCTION: SunximciDevReadOnly, LINE: 846:
SunximciMmcInit: success.
DeviceManagerStart end ...
virtual_serial_init start ...
Ns16550HostInit
virtual_serial_init end ...
system_console_init start ...
Ns16550HostSetTransMode
system_console_init end
hilog ringbuffer full, drop 6 line(s) log
[ERR][KProcess:SystemInit]Cannot find bootargs!
parse bootargs error!
[ERR][KProcess:SystemInit]Cannot find root!hilog ringbuffer full, drop 8 line(s) log
mount rootfs error!
OsMountRootfs end ...
OsUserInitProcess start ...
OsUserInitProcess end ...
[ERR][Init:thread0][VFS]lookup failed, invalid path err = -22
hilog ringbuffer full, drop 11 line(s) log
```

### 加载文件系统

加载文件系统分为两种模式, 分别是将文件系统加载到emmc中和sd卡中, 加载到emmc中需要使用**网线或者网口转接器将开发板和主机**相连, 加载到sd卡中只需要**读卡器**即可, 接下来将分别介绍两种方式

#### emmc

1. **安装tftp**

```shell
1. 安装tftp
sudo apt-get install tftp-hpa tftpd-hpa

2. 创建tftp文件夹, 存放需要传输到板子上的文件
mkdir tftp
sudo chmod -R 777 tftp

3. 编辑tftp配置文件
sudo vim /etc/default/tftpd-hpa

## 文件内容如下 ##
# /etc/default/tftpd-hpa

TFTP_USERNAME="tftp"  						
TFTP_DIRECTORY="/home/mayber/tftp"			# 第2步创建的文件夹的绝对路径
TFTP_ADDRESS=":69"							# tftp 占用主机69端口
TFTP_OPTIONS="-l -c -s"						# -l : 不从inted启动, 所以不用安装xinetd
											# -c : 可创建新文件, 默认情况下只允许覆盖原文件, 不是创建新文件
											# -s : 改变tftp的启动目录, 客户端使用时不在需要输入绝对路径
##

4. 重启tftp服务
sudo service tftpd-hpa restart


5. 验证tftp是否启动成功
netstat -a | grep tftp

## 结果如下 ##
udp        0      0 0.0.0.0:tftp            0.0.0.0:*
udp6       0      0 [::]:tftp               [::]:*
##

# 注: 在板子上uboot中请求主机中tftp文件夹中相关文件时, 若无响应, 可以检查主机上的tftp文件是否正常启动
```

2. **配置网络IP**

```shell
# 开发板需要和主机在同一个网段内, 才能完成通信, 这里仅给出使用网口转换器的配置方法
1. 检查usb网卡是否被加载使用
sudo lshw -numeric -C net

## 例子:结果 ##
 *-network:1 DISABLED
       description: Ethernet interface
       physical id: 2
       bus info: usb@2:1
       logical name: enxf8e43bc21037
       serial: f8:e4:3b:c2:10:37
       size: 1Gbit/s
       capacity: 1Gbit/s
##
如果网卡显示DISABLE, 则需要激活无线网卡
sudo ip link set enxf8e43bc21037 up

# 注:如果使用wsl, 需要重新编译内核添加对应无线网卡的驱动, 如果是虚拟机需要做好穿透设置.

2. 配置网卡ip
sudo ip addr add 192.168.50.101/24 broadcast 192.168.50.255 dev enxf8e43bc21037

3. 进入uboot
首先执行在源码目录执行命令启动内核,并在picocom中uboot信息倒计时结束之前按回车进入uboot
# 加载内核命令
sudo ./sunxi-tools/sunxi-fel uboot ./u-boot/u-boot-sunxi-with-spl.bin write 0x42000000 uImage write 0x43100000 boot.scr
# 观察串口信息命令
sudo picocom -b 115200 /dev/ttyUSB0

4. 配置uboot网络参数
# 在uboot中执行下列命令配置网关
setenv ipaddr 192.168.50.102
setenv gatewayip 0.0.0.0
setenv netmask 255.255.255.0
setenv serverip 192.168.50.101

5. 验证网络是否配置成功, ping在第二步中设置的ip
ping 192.168.50.101				
# 结果:
# Using ethernet@1c50000 device
# host 192.168.50.101 is alive
# 
# 结果如上证明配置成功, 注意由于uboot自身问题, 只有开发板能够ping通主机, 主机是ping不通开发板的
# 只要后续tftp使用没有问题即可

# 注: uboot窗口不要关, 后续仍需要使用
```

3. 将文件系统写入emmc

```shell
1. 首先创建bootargs, 并将需要的img复制到tftp服务器文件夹
cd tftp
echo -ne "bootargs=root=emmc fstype=vfat  rootaddr=10M rootsize=20M useraddr=40M usersize=100M"'\0' > bootargs
cp ${bananapi_code}/out/bpi_m2_ultra/bpi_m2_ultra_demo/rootfs_vfat.img ${bananapi_code}/out/bpi_m2_ultra/bpi_m2_ultra_demo/userfs_vfat.img .

2. 在上一步的uboot的窗口获取文件并写入emmc
# 执行以下命令
mmc list #查看当前mmc设备列表
mmc dev 1 #切换到emmc (注意这里的设备号, 一定要切换到emmc, 而不是sd卡)
mmcinfo #查看emmc信息

3. 使用tftp下载文件到内存并写入emmc
# bootargs
mw 0x42000000  0 0x32000 #清理一片内存
tftp 0x42000000  bootargs #将需要的文件加载到内存
mmc write 0x42000000  0x400 0x2 #bootargs内存写入emmc 0x400*0x200 512K

# rootfs_vfat.img
tftp 0x42000000  rootfs_vfat.img #将需要的文件加载到内存 rootfs_vfat.img 20M
mmc write 0x42000000  0x5000 0xA000

# userfs_vfat.img
tftp 0x42000000  userfs_vfat.img #将需要的文件加载到内存 rootfs_vfat.img 100M
mmc write 0x42000000  0x14000 0x32000

4. 重新启动内核
按下板子的reset键或者插拔usb线, 之后再源码目录下重新启动内核

# 加载内核命令
sudo ./sunxi-tools/sunxi-fel uboot ./u-boot/u-boot-sunxi-with-spl.bin write 0x42000000 uImage write 0x43100000 boot.scr
# 观察串口信息命令
sudo picocom -b 115200 /dev/ttyUSB0


## 正确串口信息 ##
main core booting up...
cpu 0 entering scheduler
dev mem init ...
DeviceManagerStart start ...
SunximciMmcInit: proc init success.
SunximciMmcInit: success.
FUNCTION: SunximciDevReadOnly, LINE: 846:
[ERR][KProcess:SdWorkerThread]Do not support the partition type!
No MBR detected.
SunximciMmcInit: success.
DeviceManagerStart end ...
virtual_serial_init start ...
Ns16550HostInit
virtual_serial_init end ...
system_console_init start ...
Ns16550HostSetTransMode
system_console_init end
hilog ringbuffer full, drop 6 line(s) log
warning: /dev/mmcblk0 lost, force umount ret = -6
OsMountRootfs end ...
OsUserInitProcess start ...
OsUserInitProcess end ...
[ERR]Unsupported API tcgetpgrp
hilog ringbuffer full, drop 8 line(s) log
OHOS:/$
## 
# 可以看到能够正确加载文件系统并且进入shell
```

#### SD卡

加载到sd卡上不需要使用tftp以及网线, 只需要读卡器和正确的分区表即可, 使用dd命令将相关文件写入SD卡对应位置即可, 但是使用SD卡启动, **内核以及配置文件需要进行相关更改**, 具体更改如下:

+ 配置文件更改

1. 修改mmc的hcs配置文件
必须将需要加载mmc hostId设置为0

```shell
# patch文件如下: 
diff --git a/bpi_m2_ultra/liteos_a/hdf_config/mmc/mmc_config.hcs b/bpi_m2_ultra/liteos_a/hdf_config/mmc/mmc_config.hcs
index 41bf56e..68c3bf9 100644
--- a/bpi_m2_ultra/liteos_a/hdf_config/mmc/mmc_config.hcs
+++ b/bpi_m2_ultra/liteos_a/hdf_config/mmc/mmc_config.hcs
@@ -26,7 +26,7 @@ root {

             mmc_controller_0x01c0f000 :: mmc_controller {
                 match_attr = "r40_mmc_sd";
-                hostId = 1;
+                hostId = 0;
                 devType = 1;//sd
                 irqNum = 64;
                 regBasePhy = 0x01c0f000;
@@ -44,7 +44,7 @@ root {
             }
             mmc_controller_0x01c11000 :: mmc_controller {
                 match_attr = "r40_mmc_emmc";
-                hostId = 0;
+                hostId = 2;
                 devType = 0;//emmc
                 irqNum = 66;
                 regBasePhy = 0x01C11000;
```

+ 内核文件更改

1. 修改los_get_mmcdisk_bytype接口
   如果是EMMC该结构入参需要设置为EMMC
   如果是SD该结构入参需要设置为OTHERS

2. 修改AddEmmcParts接口
     在AddEmmcParts函数中调用StorageBlockGetEmmc()函数收增加一行
       原因是使用SD情况下StorageBlockGetEmmc获取的最大扇区数不正确需要从emmcDisk中更新正确扇区

      emmc->sector_count = emmcDisk->sector_count;

```shell
# patch文件如下:
diff --git a/fs/rootfs/los_bootargs.c b/fs/rootfs/los_bootargs.c
index c6e2a680..c42169f0 100644
--- a/fs/rootfs/los_bootargs.c
+++ b/fs/rootfs/los_bootargs.c
@@ -59,7 +59,7 @@ INT32 LOS_GetCmdLine(VOID)
     }
 
 #ifdef LOSCFG_STORAGE_EMMC
-    los_disk *emmcDisk = los_get_mmcdisk_bytype(EMMC);
+    los_disk *emmcDisk = los_get_mmcdisk_bytype(OTHERS);
     if (emmcDisk == NULL) {
         PRINT_ERR("Get EMMC disk failed!\n");
         goto ERROUT;
diff --git a/fs/rootfs/los_rootfs.c b/fs/rootfs/los_rootfs.c
index a81c0163..fcb7ad15 100644
--- a/fs/rootfs/los_rootfs.c
+++ b/fs/rootfs/los_rootfs.c
@@ -61,7 +61,7 @@ STATIC INT32 AddEmmcParts(INT32 rootAddr, INT32 rootSize, INT32 userAddr, INT32
 {
     INT32 ret;
 
-    los_disk *emmcDisk = los_get_mmcdisk_bytype(EMMC);
+    los_disk *emmcDisk = los_get_mmcdisk_bytype(OTHERS);
     if (emmcDisk == NULL) {
         PRINT_ERR("Get EMMC disk failed!\n");
         return LOS_NOK;
@@ -75,6 +75,7 @@ STATIC INT32 AddEmmcParts(INT32 rootAddr, INT32 rootSize, INT32 userAddr, INT32
     }
 
     struct disk_divide_info *emmc = StorageBlockGetEmmc();
+    emmc->sector_count = emmcDisk->sector_count;
     ret = add_mmc_partition(emmc, rootAddr / EMMC_SEC_SIZE, rootSize / EMMC_SEC_SIZE);
     if (ret != LOS_OK) {
         PRINT_ERR("Failed to add mmc root partition!\n");
```

完成代码文件更改后, 可以开始进行SD卡文件系统加载

1. 重新在源码目录编译内核
2. 重新制作内核启动镜像(参考编译运行内核)
3. 将bootargs, rootfs_vfat.img, userfs_vfat.img 写入 SD卡

```shell
# 制作sd卡启动文件
1. 使用读卡器插入系统, 确认sd卡设备编号, 可以使用如下命令查看所有磁盘设备
sudo fdisk -l

## SD卡 设备信息例子如下 ##
Disk /dev/sde: 59.48 GiB, 63864569856 bytes, 124735488 sectors
Disk model: USB3.0 CRW   -SD
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 3DD76834-318B-4751-AA8C-9BC275AB0D0B

Device         Start       End   Sectors  Size Type
/dev/sde1       2048 124733353 124731306 59.5G Microsoft basic data
/dev/sde2  124733354 124735401      2048    1M Microsoft basic data
##
# 同时确认分区表相关信息, 确认分区表格式为gdt, 并且分区正确, 如果需要更改分区可以使用fdisk工具
2. 格式化sd卡(可选)
运行sudo fdisk / dev / sdx命令删除SD卡的所有分区.
运行mkfs -t vfat / dev / sdx命令将整个SD卡格式化为FAT.（x应根据您的SD卡节点更换）

3. 将对应文件写入SD卡
sudo dd if=bootargs of=/dev/sde conv=notrunc seek=1024 count=2 bs=1b

sudo dd if=rootfs_vfat.img of=/dev/sde conv=notrunc seek=20480 count=40960 bs=1b

sudo dd if=userfs_vfat.img of=/dev/sde conv=notrunc seek=81920 count=204800 bs=1b

4. 重启开发板以及内核, 检查启动信息是否正确
```

#### 备注

这里其实可以固定下开发流程;

如果对内核进行开发, 就重新编译制作uImage;

如果是要对内核进行测试, 更新文件系统的话, 只需要在编译后将新的rootfs_vfat.img 写入固定位置即可.

### 制作内核镜像并烧录到SD卡中

这里使用内核镜像直接烧录到sd卡中, 可以不再使用usb数据线, sunxi-fel工具加载内核, 而是直接从sd卡中读取.

```shell
1.首先确定SD卡的设备号
sudo fdisk -l
这里我的设备号就是/dev/sdd, 后续均会用sdd指代, 需按照自己的实际情况修改

2.清空sd卡最前面的1M空间,写入相应的文件
# 这1M空间用来装uboot内核以及bootargs
# 清空空间
sudo dd if=/dev/zero of=/dev/sdd bs=1M count=1

# 写入u-boot内核
sudo dd if=./u-boot/u-boot-sunxi-with-spl.bin of=/dev/sdd bs=1024 seek=8

# 创建新的bootargs并写入
echo -ne "bootargs=root=emmc fstype=vfat  rootaddr=20M rootsize=20M useraddr=40M usersize=100M"'\0' > bootargs

sudo dd if=bootargs of=/dev/sdd conv=notrunc seek=1024 count=2 bs=1b

3. 制作内核启动分区
sudo fdisk /dev/sdd

n->默认p->默认1->默认2048->34815					# 第一个分区大小为16M, 存放uImage以及u-boot启动参数
n->默认p->默认2->默认34816->默认剩余全部			# 第二个分区存放文件系统
p->t->c->回车(更改文件系统格式为fat)			     # 更改第一个分区文件格式为 fat
w												# 将更改写入sd卡		

4. 格式化两个分区
sudo mkfs.vfat /dev/sdd1
sudo mkfs.ext4 /dev/sdd2

5. 制作新的boot.scr, 并将uImage和boot.scr写入sdd1
cat <<EOF > boot_liteos.cmd
load mmc 0:1 0x42000000 uImage
bootm start
bootm loados
bootm go
EOF

./u-boot/tools/mkimage -T script -A arm -O linux -C none -a0 -e0 -n "boot_liteos_scr" -d boot_liteos.cmd boot.scr

# 挂载sdd1并将内核和uboot脚本写入
 mkdir sd1
 sudo mount /dev/sdd1 sd1
 
 sudo cp boot.scr uImage sd1
 
 sudo umount sd1
 
 6. 将文件系统写入sd卡
# 注意这里将文件系统直接写进了sdd 20M 和 40M 的位置这是和bootargs里的参数相对应的
# 同时这里也是为了避开sdd1的16M, 直接根据地址计算将文件系统写到了分区2
sudo dd if=rootfs_vfat.img of=/dev/sdd conv=notrunc seek=40960 count=40960 bs=1b

sudo dd if=userfs_vfat.img of=/dev/sdd conv=notrunc seek=81920 count=204800 bs=1b

7. 将sd卡插入开发板, 重启并观察串口信息, 进入shell就算成功

8. 注
# 这里bootargs的写入位置由内核决定, 内核会去SD卡的512K的位置读取bootargs, 由于uboot内核+8kb 并没有达到512K, 所以不存在写入冲突, 而第一个分区又是从sd卡1M处开始的,所以互不构成影响

# 后续如果要更换调试操作系统的相关部件, 只需要更新对应位置的内容即可, 开发更加方便

# 当然如果不想插Usb线的话, 一定要给开发板插上电源线
```

